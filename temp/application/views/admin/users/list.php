    <div class="container top">

      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
        </h2>
      </div>
      
      <div class="row">
        <div class="span12 columns">
         
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="header">#</th>
                <th class="yellow header headerSortDown">Facebook ID</th>
                <th class="yellow header headerSortDown">Full Name</th>
                <th class="yellow header headerSortDown">Birthdate</th>
                <th class="yellow header headerSortDown">Mobifone</th>
                <th class="yellow header headerSortDown">Join date</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if ( ! empty( $users ) ) :
                foreach($users as $row) :

                  echo '<tr>';
                  echo '<td>'.$row->id.'</td>';
                  echo '<td>'.$row->fbId.'</td>';
                  echo '<td>'.$row->name.'</td>';
                  echo '<td>' . date('d-m-Y', strtotime( $row->dob )).'</td>';
                  echo '<td>'.$row->mobifone.'</td>';
                  echo '<td>' . date('d-m-Y H:i:s', strtotime( $row->dateCreated )).'</td>';
                  echo '<td class="crud-actions">
                    <a href="'.site_url("admin").'/users/delete/'.$row->id.'" class="btn btn-danger">delete</a>
                  </td>';
                  echo '</tr>';
                endforeach;
              endif;
              ?>      
            </tbody>
          </table>
          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
    </div>