<?php
  $optionSeries = array();

  if ( !empty( $series ) ) {

    foreach ($series as $value) {
      $optionSeries[$value->id] = $value->name;
    }
  }

  $optionquestions = array();

  if ( !empty( $categories ) ) {
    
    foreach ($categories as $value) {
      $optionquestions[$value->id] = $value->name;
    }
  }

  $answersPost = $this->input->post('answer');
  $answerSeleted = -1;

  if ( $answerSeletedPost = $this->input->post('selected') ) {

    $answerSeleted = $answerSeletedPost[0];
  }
?>
<div class="container top">
  
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo site_url("admin"); ?>">
        <?php echo ucfirst($this->uri->segment(1));?>
      </a> 
      <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
        <?php echo ucfirst($this->uri->segment(2));?>
      </a> 
      <span class="divider">/</span>
    </li>
    <li class="active">
      <a href="#">New</a>
    </li>
  </ul>
  
  <div class="page-header">
    <h2>
      Adding <?php echo ucfirst($this->uri->segment(2));?>
    </h2>
  </div>

  <?php
  //flash messages
  if(isset($flash_message)){
    if($flash_message == TRUE)
    {
      echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> new question created with success.';
      echo '</div>';       
    }else{
      echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
      echo '</div>';          
    }
  }
  ?>
  
  <?php
  //form data
  $attributes = array('class' => 'form-horizontal', 'id' => '');
  ?>

  <div class="span7">
    <?php echo validation_errors(); ?>
  </div>

  <?php
  echo form_open('admin/questions/add', $attributes);
  ?>
    <fieldset style="clear:both">
       <div class="control-group">
        <label for="inputError" class="control-label">Language</label>
        <div class="controls">
          <label class="radio inline">
            <input type="radio" name="languageType" id="vi" value="vi" checked>
            Vietnames
          </label>
          <label class="radio inline">
            <input type="radio" name="languageType" id="en" value="en">
            English
          </label>
        </div>
      </div>
       <div class="control-group">
        <label for="inputError" class="control-label">Series</label>
        <div class="controls">
          <?php 
            echo form_dropdown('seriesId', $optionSeries, set_value('seriesId'), 'class="span2"');
          ?>
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label">Category</label>
        <div class="controls">
          <?php 
            echo form_dropdown('categoryId', $optionquestions, set_value('categoryId'), 'class="span4"');
          ?>
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label">Question</label>
        <div class="controls">
          <input type="text" id="" name="question" value="<?php echo set_value('question'); ?>" class="span6" >
          <!--<span class="help-inline">Woohoo!</span>-->
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label">Sort</label>
        <div class="controls">
          <input type="text" id="" name="sort" value="<?php echo set_value('sort'); ?>" class="span1" maxlength="2">
          <!--<span class="help-inline">Woohoo!</span>-->
        </div>
      </div>
      
      <table class="table table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>answer</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <input type="radio" name="selected[]" value="0" <?php echo ($answerSeleted == 0) ? 'checked' : ''; ?> >
            </td>
            <td>
              <input type="text" name="answer[]" class="span8" value="<?php echo $answersPost[0]; ?>">
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="selected[]" value="1" <?php echo ($answerSeleted == 1) ? 'checked' : ''; ?>>
            </td>
            <td>
              <input type="text" name="answer[]" class="span8" value="<?php echo $answersPost[1]; ?>">
            </td>
          </tr>
           <tr>
            <td>
              <input type="radio" name="selected[]" value="2" <?php echo ($answerSeleted == 2) ? 'checked' : ''; ?>>
            </td>
            <td>
              <input type="text" name="answer[]" class="span8" value="<?php echo $answersPost[2]; ?>">
            </td>
          </tr>
           <tr>
            <td>
              <input type="radio" name="selected[]" value="3" <?php echo ($answerSeleted == 3) ? 'checked' : ''; ?>>
            </td>
            <td>
              <input type="text" name="answer[]" class="span8" value="<?php echo $answersPost[3]; ?>">
            </td>
          </tr>
        </tbody>
      </table>

      <div class="form-actions">
        <button class="btn btn-primary" type="submit">Save changes</button>
        <button class="btn" type="reset">Cancel</button>
      </div>
    </fieldset>

  <?php echo form_close(); ?>

</div>
     