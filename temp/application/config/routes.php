<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = 'user/index';
$route['404_override'] = '';

/*admin*/
$route['admin'] = 'user/index';
$route['admin/signup'] = 'user/signup';
$route['admin/create_member'] = 'user/create_member';
$route['admin/login'] = 'user/index';
$route['admin/logout'] = 'user/logout';
$route['admin/login/validate_credentials'] = 'user/validate_credentials';
$route['admin/users/(:num)'] = 'admin/users/index/$1'; //$1 = page number

// resource series
$route['admin/series'] = 'admin/series/index';
$route['admin/series/add'] = 'admin/series/add';
$route['admin/series/update'] = 'admin/series/update';
$route['admin/series/update/(:any)'] = 'admin/series/update/$1';
$route['admin/series/delete/(:any)'] = 'admin/series/delete/$1';

// resource categories
$route['admin/categories'] = 'admin/categories/index';
$route['admin/categories/add'] = 'admin/categories/add';
$route['admin/categories/update'] = 'admin/categories/update';
$route['admin/categories/update/(:any)'] = 'admin/categories/update/$1';
$route['admin/categories/delete/(:any)'] = 'admin/categories/delete/$1';

// resource questions
$route['admin/questions'] = 'admin/questions/index';
$route['admin/questions/(:num)'] = 'admin/questions/index/$1'; //$1 = page number
$route['admin/questions/add'] = 'admin/questions/add';
$route['admin/questions/update'] = 'admin/questions/update';
$route['admin/questions/update/(:any)'] = 'admin/questions/update/$1';
$route['admin/questions/delete/(:any)'] = 'admin/questions/delete/$1';

// resource api players
$route['api/players'] = ($_SERVER['REQUEST_METHOD'] == 'POST') ? 'api/players/add' : '';
$route['api/players/(:any)'] = ($_SERVER['REQUEST_METHOD'] == 'PUT') ? 'api/players/update/$1' : '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */