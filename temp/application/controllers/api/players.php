<?php
require( APPPATH . '/libraries/REST_Controller.php' );

class Players extends REST_Controller {
	
	public function __construct() {

		parent::__construct();
		$this->load->model( 'player' );
	}
	
	/**
	 * get all player
	 * GET /api/players
	 * 	or /api/players/5/10
	 */
	public function getAll_get( $limit = 10, $offset = 0 ) {

		// call getAll function from player model
		$result = $this->player->getAll( $offset, $limit );

		// format result before return
		$this->formatResponsePlayer( $result );
		
	}

	/**
	 * get One player by id
	 * GET /api/players/1
	 */
	public function getOne_get( $id ) {

		// call getById function from player model
		$result = $this->player->getById( $id );

		// format result before return
		$this->formatResponsePlayer( $result );
	}

	/**
	 * Insert a players
	 * POST /API/players
	 */
	public function add_post() {

		// call add function from player model
		$result = $this->player->add( $this->post() );

		// format result before return
		$this->formatResponse( $result );
	}

	/**
	 * Update a player
	 * PUT /API/players/1aadf3qer
	 */
	public function update_put( $fbId ) {

		// call edit function from player model
		$result = $this->player->edit( $fbId, $this->put() );

		// format result before return
		$this->formatResponse( $result );
	}


	/**
	 * format result before return
	 * @param  array or object $result
	 * @return json
	 */
	protected function formatResponse( $result) {

		// in the case error or getAll
		if ( is_array( $result ) ) {
			
			// get element last is statusCode and remove it from result
			$status = array_pop( $result );
		}
		else {

			// get statusCode
			$status = $result->statusCode;

			// remove statusCode element
			unset($result->statusCode);
		}

		$this->response( $result, $status );
	}

    /**
     * login user
     * @return json
     */
    public function login_post() {

    	// call login function from player model
    	$result = $this->player->login( $this->post() );

    	// format result before return
    	$this->formatResponsePlayer( $result );
    }

    /**
     * logout user
     * @return json
     */
    public function logout_get() {

    	// call logout funciton from player model
    	$result = $this->player->logout();

    	// format result before return
    	$this->formatResponsePlayer( $result );
    }

    /**
     * get current play period, game counts and position
     * @param  int $playerId
     * @return json
     */
    public function getCurrentData_get( $playerId ) {

    	$result = $this->player->getCurrentData( $playerId );

    	// format result before return
    	$this->formatResponsePlayer( $result );
    }
}