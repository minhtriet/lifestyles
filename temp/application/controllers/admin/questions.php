<?php
class Questions extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/questions';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('question');
        $this->load->model('serie');
        $this->load->model('category');
        $this->load->model('answer');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {
        $this->config->load('pagination');
        $config = $this->config->item('config');
        $config['base_url'] =  base_url().'admin/questions';

        //limit end
        $page = (int)$this->uri->segment(3);

        //math to get the initial record to be select in the database
        $offset = ($page - 1) * $config['per_page'];

        if ( $offset < 0 ) {
            $offset = 0;
        } 

        $questions = $this->question->getAll( $config['per_page'], $offset );

        $data['questions'] = $questions['questions'];
        $config['total_rows'] = $questions['count'];

        //initializate the pagination helper 
        $this->pagination->initialize($config);  

        //load the view
        $data['main_content'] = 'admin/questions/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add() {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $answers = $this->input->post('answer');
            $answers = array_filter( $answers );
            $countAnswer = count( $answers );

            if ( ! $this->input->post('selected') || $countAnswer !== 4 ) {

                $this->question->validate['answer'] = array( 
                        'field' => 'answer',
                        'label' => 'answers',
                        'rules' => "fill_all"
                    );
            }

            //form validation
            $this->form_validation->set_rules( $this->question->validate);
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ( $this->form_validation->run() ) {

                $questionData = $this->input->post();
                unset( $questionData['answer'] );
                
                //if the insert has returned true then we show the flash message
                if ( $insertId = $this->question->insert( $questionData, TRUE) ) {

                    $index = 0;
                    $answerSeletedId = 0;
                    $answerSeleted = $this->input->post('selected')[0];

                    // insert answer with $insertId question
                    foreach ($answers as $answer) {

                        $anserData = array(
                                'questionId' => $insertId,
                                'answer' => $answer,
                            );

                        $answerId = $this->answer->insert( $anserData );

                        if ( (int)$answerSeleted === $index++ ) {

                          $answerSeletedId = $answerId; 
                        }
                    }

                    $this->question->update( $insertId, array( 'answer' => $answerSeletedId ) );
                    $data['flash_message'] = TRUE;

                } else {

                    $data['flash_message'] = FALSE; 
                }
            }
        }

        //load the view
        $data['main_content'] = 'admin/questions/add';
        $data['series'] = $this->serie->get_all();
        $data['categories'] = $this->category->get_all();
        $this->load->view('includes/template', $data);  
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update() {

        //question id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $answers = $this->input->post('answer');
            $answers = array_filter( $answers );
            $countAnswer = count( $answers );

            if ( ! $this->input->post('selected') || $countAnswer !== 4 ) {
                $this->question->validate['answer'] = array( 
                        'field' => 'answer',
                        'label' => 'answers',
                        'rules' => "fill_all"
                    );
            }

            //form validation
            $this->form_validation->set_rules( $this->question->validate);
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            
            //if the form has passed through the validation
            if ($this->form_validation->run()) {

                $questionData = $this->input->post();
                unset( $questionData['answer'] );

                //if the insert has returned true then we show the flash message
                if ($this->question->update($id, $questionData, TRUE ) == TRUE) {
                    
                    $index = 0;
                    $answersId = $this->input->post('answerId');

                    foreach ( $answers as $answer ) {
                        $answerData = array(
                                'questionId' => $id,
                                'answer' => $answer
                            );
                        $this->answer->update( $answersId[$index++], $answerData );
                    }
                    $this->session->set_flashdata('flash_message', 'updated');
                } else {

                    $this->session->set_flashdata('flash_message', 'not_updated');
                }

                redirect('admin/questions/update/'.$id.'');

            }//validation run

        }

        //product data 
        $data['question'] = $this->question->get($id);
        $data['answers'] = $this->answer->get_many_by('questionId', $id);
        $data['series'] = $this->serie->get_all();
        $data['categories'] = $this->category->get_all();

        //load the view
        $data['main_content'] = 'admin/questions/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->question->delete($id);

        // delete answer by questionId
        $this->answer->delete_by( 'questionId', $id );
        redirect('admin/questions');
    }//edit

}