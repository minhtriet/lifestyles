<?php

class Users extends CI_Controller {

	 public function __construct()
    {
        parent::__construct();

        $this->load->model('player');
    }
    /**
    * Check if the user is logged in, if he's not, 
    * send him to the login page
    * @return void
    */	
	function index()
	{
		$this->config->load('pagination');
        $config = $this->config->item('config');
        $config['base_url'] =  base_url().'admin/users';

        //limit end
        $page = (int)$this->uri->segment(3);

        //math to get the initial record to be select in the database
        $offset = ($page - 1) * $config['per_page'];

        if ( $offset < 0 ) {
            $offset = 0;
        } 

        $users = $this->player->getAll( $config['per_page'], $offset );

        $data['users'] = $users['players'];
        $config['total_rows'] = $users['count'];

        //initializate the pagination helper 
        $this->pagination->initialize($config);  

        //load the view
        $data['main_content'] = 'admin/users/list';
        $this->load->view('includes/template', $data); 
	}

	function delete() {

		$id = $this->uri->segment(4);

		// delete answer by questionId
        // $this->player->delete( $id );
        redirect('admin/users');
	}
}