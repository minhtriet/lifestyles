<?php

class MY_Form_validation extends CI_Form_validation {

	// protected $params = array();

	/**
	* Retrieve the validation errors.
	*
	* @return array
	*/
	public function validation_errors()
	{
		$string = strip_tags($this->error_string());

		return explode("\n", trim($string, "\n"));
	}

	/**
	* set form data for this params
	* @return null
	*/
	public function set_params( $data )
	{
		$this->params = $data;
	}

	/**
	* reset array error to null
	* @return null
	*/
	public function reset_validation()
	{
		$this->_field_data = array();
        $this->_config_rules = array();
        $this->_error_array = array();
        $this->_error_messages = array();
        $this->error_string = '';

        return $this;
	}

	/**
	* validate date of birth
	* @param  array $dob array('bday' => 1, 'bmonth' => 12, 'byear' => 2014)
	* @return bool
	*/
	public function date_of_birth( $dob )
	{
		$isValid = FALSE;

		if ( ! is_array( $dob ) || count($dob) !== 3) {

			return $isValid;
		}
		
		$birthDate = $dob['bmonth'] . '/' . $dob['bday'] . '/' . $dob['byear'] ;

		$isValid = $this->valid_date( $birthDate );

		$this->set_message('date_of_birth', 'Birthdate is not in the correct format');
		
		return $isValid;
	}

	/**
	 * Performs a Regular Expression match test.
	 *
	 * @param    string
	 * @param    regex
	 * @return    bool
	 */
	function regex_match($str, $regex)
	{
		if ( ! preg_match($regex, $str))
		{
			return FALSE;
		}

		$this->set_message('regex_match', 'The %s field is not in the correct format.');
		
		return  TRUE;
	}

	/** 
	 * validate datetime
	 * @param   string $str
	 * @return bool
	 */
	public function valid_date( $str )
	{
		
		$dateformat = 'm/d/Y';
		$date = DateTime::createFromFormat($dateformat, $str);

		$this->set_message('valid_date', 'The %s field must contain a valid date');

		return $date && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0;
	}

	public function fill_all( $answer, $count ) {

		$this->set_message( 'fill_all', 'Please fill all answer and select an answer right' );

		return FALSE;
	}

	function valid_phone( $phone )
	{

		$isValid = TRUE;

		$this->set_message( 'valid_phone', 'The %s is not in the correct format' );

		$len = strlen( $phone );

		if ( ! is_numeric( $phone ) || ( $len < 9 && $len > 11 ) ) {

			$isValid = FALSE;
		}
		
		return $isValid; 
	}
}