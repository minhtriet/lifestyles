<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Answer extends MY_Model {

    // set table is Sweepstakes
	protected $_table = 'answers';

	protected $public_attributes = array(
		'id',
		'questionId',
		'answer',
  	);
}