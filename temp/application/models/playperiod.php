<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PlayPeriod extends MY_Model {

	protected $_table = 'playperiod';

    // set validations rules
	protected $validate = array(
		'playerId' => array( 
			'field' => 'playerId', 
		   	'label' => 'playerId',
		   	'rules' => 'required|greater_than[0]'
		),
		'score' => array(
			'field' => 'score',
		   	'label' => 'score',
		   	'rules' => 'required|greater_than[0]'
		),
	);

	protected $public_attributes = array(
		'id',
		'playerId',
		'score',
		'dateCreated',
		'totalPlayTimes',
  	);

	public function getTop5() {

		$result = $this->db->select('playerId, fullname, avatar, sum(score) as score')
			->join('playperiod', 'players.id = playperiod.playerId')
			->order_by('score')
			->group_by('playerId')
			->limit(5)
			->get('players')
			->result();

		if ( empty( $result ) ) {

			return array( 'error' => 'Not found', 'statusCode' => 404 );
		}
		else {

			return array( 'playperiod' => $result, 'statusCode' => 200 );
		}
	}

	public function getWinerOnDay( $dateCreated ) {

		$dateCreated = date( 'Y-m-d', strtotime( $dateCreated ));
		$result = $this->get_by( 'dateCreated' => $dateCreated )->order_by( 'score', 'DESC')->limit( 10 );

		if ( empty( $result ) ) {

			return array( 'error' => 'Not found', 'statusCode' => 404 );
		}
		else {

			return array( 'playperiod' => $result, 'statusCode' => 200 );
		}
	}

	/**
	* get player by fbId
	* @param  int $id facebook id
	* @return array
	*/
	public function getById( $fbId ) {
		
		$result = $this->get_by( 'fbId', $fbId );

		if ( empty($result) ) {

            // return log errors when return empty result
            $errors = array( 'errors' => 'Player Not Found', 'statusCode' => 404 ); 

			return $errors; 
		} 
        else {

			$result->statusCode = 200;

            // return object
			return $result;
		}
  	}

	/**
	* add a player
	* @param array $data form post
	*/
	public function add( $data ) {

        // validate data insert 
		if ( empty( $data ) ) {

            // return log errors when data miss/ invalid
            $errors =  array( 'errors' => 'Please the required enter data', 'statusCode' => 400 );

            return $errors;
        } 
        else {

            // set rules validation
			$this->form_validation->set_rules( $this->validate );

			if ( $this->form_validation->run() === FALSE ) {

				$errors = $this->form_validation->validation_errors();

                // return result errors log
				$result = array( 'errors' => $errors, 'statusCode' => 400 );
			}
			else {

				$data['fbId'] = md5( Player::ENCRYPTION_KEY . $data['fbId'] );

				$result = $this->getById( $data['fbId'] );

				if ( ! empty( $result ) && is_object( $result) ) {

					return $result;
				}
				else {

					$data['dateCreate'] = date('Y-m-d');
					
					$insertId = $this->insert( $data, TRUE );
					
					if ( $insertId ) {

	                    // get object player by fbId
						$result = $this->getById( $data['fbId'] );
						$result->statusCode = 201;
					} 
	                else {

						// get and log error message
						$errorMessage = $this->db->_error_message();

						$result = array( 'errors' => $errorMessage, 'statusCode' => 400 );
					}
				}
			}

			return $result;
		}
	}

	/**
	* update a player by fbid
	* @param  int $fbId  facebook id
	* @param  array $data
	* @return object
	*/
	public function edit( $fbId, $data ) {

        // validate data edit
		if ( empty( $data ) ) {

            $errors = array( 'errors' => 'Please enter dob and mobifone', 'statusCode' => 400 );

            // return errors
            return $errors;
        } 
        else {

        	$errors = array();

            // validate id 
			if ( ! trim($fbId) ) {

                $errors[] = 'The fbId is required';     
			}
			
			if ( ! isset($data['mobifone']) || ! $data['mobifone'] ) {

				$errors[] = 'The mobifone field is required';
			}
			elseif ( ! $this->form_validation->valid_phone( $data['mobifone'] )) {

				$errors[] = 'The mobifone is not in the correct format';
			}

			if ( ! isset($data['dob']) || ! $data['dob'] ) {

				$errors[] = 'The dob field is required';
			}
			elseif ( ! $this->form_validation->date_of_birth( $data['dob'] ) ) {

				$errors[] = 'The dob field must contain a valid date';
			}

			if ( ! empty( $errors ) ) {

				return array( 'errors' => $errors, 'statusCode' => 400 );
			}

			$dob = $data['dob']['byear'] . '-' . $data['dob']['bmonth'] . '-' . $data['dob']['bday'];
			
			$isUpdated =$this->db->set('dob', $dob)
					->set('mobifone', $data['mobifone'])
					->where( 'fbId', $fbId)
					->update( $this->_table );

			if ( $isUpdated ) {

				$result = $this->getById( $fbId );
			} 
            else {

				// get and log error message
				$errorMessage = $this->db->_error_message();

				$result = array( 'errors' => $errorMessage, 'statusCode' => 400  );
			}

			return $result;
		}
	}
}