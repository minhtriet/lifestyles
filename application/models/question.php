<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question extends MY_Model {

    // set table is Sweepstakes
	protected $_table = 'questions';

    // set validations rules
	public $validate = array(
		'seriesId' => array( 
			'field' => 'seriesId', 
		   	'label' => 'series',
		   	'rules' => 'required'
		),
	    'categoryId' => array(
    		'field' => 'categoryId',
		   	'label' => 'category',
		   	'rules' => 'required'
		),
		'question' => array(
    		'field' => 'question',
		   	'label' => 'question',
		   	'rules' => 'required'
		),
		'point' => array(
    		'field' => 'point',
		   	'label' => 'point',
		   	'rules' => 'required|greater_than[0]'
		),
	);

	protected $public_attributes = array(
		'id',
		'seriesId',
		'categoryId',
		'question',
		'answer',
		'point',
  	);

	public function getAll( $limit, $offset ) {

		$questions = $this->limit($limit, $offset )->get_all();

		$questionCount = $this->count_all();

		return array( 'questions' => $questions, 'count' => $questionCount );
	}
}