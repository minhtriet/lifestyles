<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Serie extends MY_Model {

    // set table is Sweepstakes
	protected $_table = 'series';
	protected $before_create = array( 'converDate' );
	protected $before_update = array( 'converDate' );

    // set validations rules
	public $validate = array(
		'name' => array( 
			'field' => 'name', 
		   	'label' => 'name',
		   	'rules' => 'required'
		),
		'activeDate' => array(
			'field' => 'activeDate',
		   	'label' => 'Active Date',
		   	'rules' => 'required'
		),
	    'expireDate' => array(
    		'field' => 'expireDate',
		   	'label' => 'Expire Date',
		   	'rules' => 'required'
		),
	);

	protected $public_attributes = array(
		'id',
		'name',
		'activeDate',
		'expireDate',
  	);

	protected function converDate( $series ) {

		$series['activedate'] = date( 'Y-m-d', strtotime($series['activedate']) );
		$series['expiredate'] = date( 'Y-m-d', strtotime($series['expiredate']) );

		return $series;
	}

}