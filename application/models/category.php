<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Model {

    // set table is Sweepstakes
	protected $_table = 'category';

    // set validations rules
	public $validate = array(
		'name' => array( 
			'field' => 'name', 
		   	'label' => 'name',
		   	'rules' => 'required'
		),
		'type' => array(
    		'field' => 'type',
		   	'label' => 'Category',
		   	'rules' => 'required'
		),
	);

	protected $public_attributes = array(
		'id',
		'name',
		'type',
  	);

}