<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PlayPeriod extends MY_Model {

	protected $_table = 'playperiod';

	protected $public_attributes = array(
		'id',
		'playerId',
		'score',
		'dateCreated',
		'totalPlayTimes',
  	);

	public function getTop5() {

		$result = $this->db->select('playerId, name, avatar, sum(score) as score')
			->join('playperiod', 'players.id = playperiod.playerId')
			->order_by('score', 'DESC')
			->group_by('playerId')
			->limit(5)
			->get('players' )
			->result();

		return $result;
	}

	public function getWinerOnDay( $limit = 0, $dateCreated = null ) {

		$dateCreated = $dateCreated ? $dateCreated : date('m/d/Y');

		// if ( ! $this->form_validation->valid_date( $dateCreated ) ) {

		// 	return array( 'error' => 'The date field must contain a valid date (m/d/Y)', 'statusCode' => 400 );
		// }

		$dateCreated = date( 'Y-m-d', strtotime( str_replace( '-', '/', $dateCreated ) ) );
		
		$query = $this->db->select('playerId, name, avatar, sum(score) as score, dob, mobifone, players.dateCreated')
			->join('playperiod', 'players.id = playperiod.playerId')
			->order_by('score', 'DESC')
			->group_by('playerId')
			->where( 'playperiod.dateCreated', $dateCreated );

		if ( $limit ) {
			$query->limit( $limit );
		}

		$result = $query->get('players' )->result();

		return $result;
	}

	/**
	* get playperiod
	* @return array
	*/
	public function getCurrent() {
		
		$playerId = $this->session->userdata( 'playerId' );

		$result = $this->get_by( array( 'playerId' => $playerId, 'dateCreated' => date( 'Y-m-d') ) );

        // return object
		return $result;
  	}

  	public function getTotalPlayTimes() {

  		$result = $this->getCurrent();

  		if ( empty( $result ) ) {

  			return 0;
  		}
  		else {

  			return (int)$result->totalPlayTimes;
  		}
  	}

  	protected function calculationScore( $data ) {

  		$score = 0;

  		// array( array( 'questionId' => array(point, answer ) ) )
  		$questionAnswers = $this->session->userdata( 'questionAnswers');

  		// destroy session
  		$this->session->set_userdata( 'questionAnswers', null );

  		$remainTime = isset( $data['remainTime'] ) ? (int)$data['remainTime'] : 0;

  		if ( $remainTime > 85 ) {

  			return $score;
  		}

  		$answeredCorrect = 0;

  		if ( ! empty( $questionAnswers ) && isset( $data['answers'] ) ) {

  			foreach ( $data['answers'] as $key => $answer ) {

  				$question = $questionAnswers[$key];

  				if ( (int)$answer[0] == (int)$question[1] ) {

  					$answeredCorrect++;
  					$score += (int)$question[0];
  				}
  			}
  		}

  		$this->session->set_userdata( 'kq', array( 'knowledgePoints' => $score, 'bonusPoints' => $remainTime, 'answeredCorrect' => $answeredCorrect ) );

  		$score += $remainTime;

  		return $score;
  	}

	/**
	* add a playperiod
	* @param array $data form post
	*/
	public function add( $data ) {

		$score = $this->calculationScore( $data );

		$result = $this->getCurrent();

		// exists in date
		if ( ! empty( $result ) ) {

			if ( $result->totalPlayTimes < 3 ) {

				$playPeriodData = array( 'score' => $result->score + $score, 'totalPlayTimes' => $result->totalPlayTimes + 1 );

				$isUpdated = $this->update( $result->id, $playPeriodData );
			}
			else {

				return FALSE;
			}
		}
		else {

			$playPeriodData = array(
				'playerId' => $this->session->userdata( 'playerId' ),
				'score' => $score,
				'dateCreated' => date( 'Y-m-d' )
			);

			$insertId = $this->insert( $playPeriodData, TRUE );
		}
	}

	public function getQuestions() {

		$this->load->model( 'serie' );

		$serie = $this->serie->limit( 1 )->order_by( 'activeDate', 'ASC' )->get_all();

		$this->db->select('questions.*, category.type');
		$this->db->join('category', 'questions.categoryId = category.id');

		if ( ! empty( $serie ) ) {

			$serie = $serie[0];

			if ( strtotime( $serie->expireDate ) >= strtotime('now') ) {

				$this->db->where( 'seriesId', $serie->id );
			}
			else {
				$this->db->where( 'seriesId !=', $serie->id );
			}
		}

		$query = $this->db->get( 'questions' );
		$questions = $query->result();

		if ( ! empty( $questions ) ) {

			$brands = array();
			$loves = array();

			foreach ($questions as $key => $question ) {
				
				if ( $question->type === 'brand' ) {
					array_push( $brands, $question );
				}
				else {
					array_push( $loves, $question );
				}
			}

			$brandsCount = count( $brands );
			$lovesCount = count( $loves );
			$questions = array();
			$questionsId = array();

			if ( $brandsCount ) {
				if ( $brandsCount < 3 ) {

					foreach ($brands as $key => $value) {
						$questions[] = $value;
						$questionsId[] = $value->id;
					}
					
				}
				else {
					$q1 = $this->randExcept( 0, $brandsCount - 1 );
					$q2 = $this->randExcept( 0, $brandsCount - 1, array( $q1 ) );
					$questions[] = $brands[$q1];
					$questions[] = $brands[$q2];
					$questionsId[] = $brands[$q1]->id;
					$questionsId[] = $brands[$q2]->id;
				}
			}

			if ( $lovesCount ) {
				if ( $lovesCount < 4 ) {

					foreach ($loves as $key => $value) {
						$questions[] = $value;
						$questionsId[] = $value->id;
					}
				}
				else {
					$q3 = $this->randExcept( 0, $lovesCount - 1 );
					$q4 = $this->randExcept( 0, $lovesCount - 1, array( $q3 ) );
					$q5 = $this->randExcept( 0, $lovesCount - 1, array( $q3, $q4 ) );
					$questions[] = $loves[$q3];
					$questions[] = $loves[$q4];
					$questions[] = $loves[$q5];
					$questionsId[] = $loves[$q3]->id;
					$questionsId[] = $loves[$q4]->id;
					$questionsId[] = $loves[$q5]->id;
				}
			}
			$answers = $this->db->select('*')
					->where_in( 'questionId', $questionsId )
					->get( 'answers')->result();

			$questionAnswers = array();

			foreach ( $questions as $key => $q ) {
					
				$questionAnswers[$q->id] = array( $q->point, $q->answer );
			}

			$this->session->set_userdata( 'questionAnswers', $questionAnswers );

			return array( $questions, $answers );
		}
		else {

			return array( null, null );
		}
	}

	protected function randExcept($min, $max, $excepting = array()) {

	    $num = mt_rand($min, $max);

	    return in_array($num, $excepting) ? $this->randExcept($min, $max, $excepting) : $num;
	}

	public function completed() {

		$fbId = $this->session->userdata('fbId');

		$player = $this->db->select('*')
					->where('fbId', md5( $fbId ))
					->where('mobifone IS NOT NULL')
					->where('dob IS NOT NULL')
					->get('players')
					->result();

		return (int)!(empty($player));
	}

	public function certify() {

		$fbId = $this->session->userdata('fbId');

		$player = $this->db->select('*')
			->where('fbId', md5( $fbId ))
			->get('players')
			->row();

		$kq = $this->session->userdata( 'kq' );
		$xepLoai = 'fail';

		if ( in_array( $kq['answeredCorrect'], array( 3, 4 ) ) ) {
			$xepLoai = 'Giỏi';
		}
		else if ( $kq['answeredCorrect'] == 5 ) {
			$xepLoai = 'Xuất sắc';
		}

		$result = array( 'name' => $player->name,
			'avatar' => $player->avatar,
			'point' => $kq['knowledgePoints'] + $kq['bonusPoints'],
			'xeploai' => $xepLoai
		);

        $this->session->set_userdata( 'kq', null );

		return $result;
	}
}