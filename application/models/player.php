<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends MY_Model {

	const ENCRYPTION_KEY = "fbId&^%#@daw";

    // set table is players
	protected $_table = 'players';

    // set validations rules
	protected $validate = array(
        // verify name must be is required
		'name' => array( 
		   	'field' => 'name',
			'label' => 'name', 
		   	'rules' => 'required'
		),
		'email' => array( 
			'field' => 'email', 
		   	'label' => 'email',
		   	'rules' => 'required'
		),
		'fbId' => array(
			'field' => 'fbId',
		   	'label' => 'fbId',
		   	'rules' => 'required'
		),
	);

	protected $public_attributes = array(
		'id',
		'fbId',
		'name',
		'email',
		'dob',
		'mobifone',
		'avatar',
  	);

	/**
	* get player by fbId
	* @param  int $id facebook id
	* @return array
	*/
	public function getById( $fbId ) {
		
		$result = $this->get_by( 'fbId', $fbId );

		if ( empty($result) ) {

            // return log errors when return empty result
            $errors = array( 'errors' => 'Player Not Found', 'statusCode' => 404 ); 

			return $errors; 
		} 
        else {

        	return array(
        			'token' => md5( Player::ENCRYPTION_KEY ) . $result->fbId,
        			'playerId' => $result->id,
        			'statusCode' => 200
        		);
		}
  	}

  	public function getAll( $limit, $offset ) {

		$players = $this->limit($limit, $offset )->get_all();

		$count = $this->count_all();

		return array( 'players' => $players, 'count' => $count );
	}

	/**
	* add a player
	* @param array $data form post
	*/
	public function add( $data ) {

		$data['fbId'] = md5( $data['fbId'] );

		$isExists = $this->get_by( 'fbId', $data['fbId'] );

		if ( $isExists ) {

			$this->session->set_userdata( array( 'playerId' => $isExists->id ) );
			return TRUE;
		}
		else {

			$data['dateCreate'] = date('Y-m-d');
			
			$insertId = $this->insert( $data, TRUE );
			$this->session->set_userdata( array( 'playerId' => $insertId ) );

			return $insertId;
		}
	}

	/**
	* update a player by fbid
	* @param  int $fbId  facebook id
	* @param  array $data
	* @return object
	*/
	public function edit( $data ) {

		$fbId = $this->session->userdata('fbId');

        // validate data edit
		// if ( empty( $data ) ) {

  //           $errors = array( 'errors' => 'Please enter dob and mobifone', 'statusCode' => 400 );

  //           // return errors
  //           return $errors;
  //       } 
  //       else {

  //       	$errors = array();

  //           // validate id 
		// 	if ( ! trim($fbId) ) {

  //               $errors[] = 'The fbId is required';     
		// 	}
			
		// 	if ( ! isset($data['mobifone']) || ! $data['mobifone'] ) {

		// 		$errors[] = 'The mobifone field is required';
		// 	}
		// 	elseif ( ! $this->form_validation->valid_phone( $data['mobifone'] )) {

		// 		$errors[] = 'The mobifone is not in the correct format';
		// 	}

		// 	if ( ! isset($data['dob']) || ! $data['dob'] ) {

		// 		$errors[] = 'The dob field is required';
		// 	}
		// 	elseif ( ! $this->form_validation->date_of_birth( $data['dob'] ) ) {

		// 		$errors[] = 'The dob field must contain a valid date';
		// 	}

		// 	if ( ! empty( $errors ) ) {

		// 		return array( 'errors' => $errors, 'statusCode' => 400 );
		// 	}

			$dob = $data['dob']['byear'] . '-' . $data['dob']['bmonth'] . '-' . $data['dob']['bday'];
			
			$isUpdated =$this->db->set('dob', $dob)
					->set('mobifone', $data['mobifone'])
					->where( 'fbId', md5($fbId))
					->update( $this->_table );

			return $isUpdated;
		// }
	}
}