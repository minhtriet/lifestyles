    <div class="container top">

      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(3));?> 
        </h2>
      </div>
      
      <div class="row">
        <div class="span12 columns">
          
          <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            
            echo form_open('admin/users/winner', $attributes);
     
              echo form_label('Date:', 'searchDate');
              echo '<input type="text" id="searchDate" class="span2" readonly name="searchDate" value="'. $searchDate .'" >';

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              echo form_submit($data_submit);

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="header">#</th>
                <th class="header">Avatar</th>
                <th class="yellow header headerSortDown">Full Name</th>
                <th class="yellow header headerSortDown">Score</th>
                <th class="yellow header headerSortDown">Birthdate</th>
                <th class="yellow header headerSortDown">Mobifone</th>
                <th class="yellow header headerSortDown">Join date</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              if ( ! empty( $users ) ) :
                $index = 1;
                foreach($users as $row) :

                  echo '<tr>';
                  echo '<td>'.$index++.'</td>';
                  echo '<td><img src="'.$row->avatar.'" style="height:27px"></td>';
                  echo '<td>'.$row->name.'</td>';
                  echo '<td>'.$row->score.'</td>';
                  echo '<td>' . date('d-m-Y', strtotime( $row->dob )).'</td>';
                  echo '<td>'.$row->mobifone.'</td>';
                  echo '<td>' . date('d-m-Y H:i:s', strtotime( $row->dateCreated )).'</td>';
                  echo '</tr>';
                endforeach;
              endif;
              ?>      
            </tbody>
          </table>
          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
    </div>