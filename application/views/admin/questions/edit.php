<?php
  $answerSeleted = isset($question->answer) ? $question->answer : 0;
  $optionSeries = array();

  if ( !empty( $series ) ) {

    foreach ($series as $value) {
      $optionSeries[$value->id] = $value->name;
    }
  }

  $optionquestions = array();

  if ( !empty( $categories ) ) {
    
    foreach ($categories as $value) {
      $optionquestions[$value->id] = $value->name;
    }
  }

?>
<div class="container top">
  
  <ul class="breadcrumb">
    <li>
      <a href="<?php echo site_url("admin"); ?>">
        <?php echo ucfirst($this->uri->segment(1));?>
      </a> 
      <span class="divider">/</span>
    </li>
    <li>
      <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
        <?php echo ucfirst($this->uri->segment(2));?>
      </a> 
      <span class="divider">/</span>
    </li>
    <li class="active">
      <a href="#">Update</a>
    </li>
  </ul>
  
  <div class="page-header">
    <h2>
      Updating <?php echo ucfirst($this->uri->segment(2));?>
    </h2>
  </div>


  <?php
  //flash messages
  if($this->session->flashdata('flash_message')){
    if($this->session->flashdata('flash_message') == 'updated')
    {
      echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> question updated with success.';
      echo '</div>';       
    }else{
      echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
      echo '</div>';          
    }
  }
  ?>
  
  <?php
  //form data
  $attributes = array('class' => 'form-horizontal', 'id' => '');

  //form validation
  echo validation_errors();
  echo form_open('admin/questions/update/'.$this->uri->segment(4).'', $attributes);
  ?>
  <fieldset style="clear:both">
   <div class="control-group">
    <label for="inputError" class="control-label">Series</label>
    <div class="controls">
      <?php 
        echo form_dropdown('seriesId', $optionSeries, $question->seriesId, 'class="span2"');
      ?>
    </div>
  </div>
  <div class="control-group">
    <label for="inputError" class="control-label">Category</label>
    <div class="controls">
      <?php 
        echo form_dropdown('categoryId', $optionquestions, $question->categoryId, 'class="span4"');
      ?>
    </div>
  </div>
  <div class="control-group">
    <label for="inputError" class="control-label">Question</label>
    <div class="controls">
      <input type="text" id="" name="question" value="<?php echo $question->question; ?>" class="span10" >
      <!--<span class="help-inline">Woohoo!</span>-->
    </div>
  </div>
  <div class="control-group">
    <label for="inputError" class="control-label">Point</label>
    <div class="controls">
      <input type="text" id="point" name="point" value="<?php echo $question->point; ?>" class="span1" >
      <!--<span class="help-inline">Woohoo!</span>-->
    </div>
  </div>

  <table class="table table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>answer</th>
      </tr>
    </thead>
    <tbody>
      <?php if ( ! empty( $answers ) ) :
        foreach ( $answers as $answer ) :
      ?>
        <tr>
          <td>
            <input type="radio" name="selected[]" value="<?php echo $answer->id; ?>" <?php echo ($answerSeleted == $answer->id) ? 'checked' : ''; ?> >
          </td>
          <td>
            <input type="hidden" value="<?php echo $answer->id; ?>" name="answerId[]">
            <input type="text" name="answer[]" class="span8" value="<?php echo $answer->answer; ?>">
          </td>
        </tr>
      <?php
        endforeach;
       endif;?>
       
    </tbody>
  </table>

  <div class="form-actions">
    <button class="btn btn-primary" type="submit">Save changes</button>
    <a class="btn" href="<?php echo site_url('admin/questions')?>">Cancel</a>
  </div>
</fieldset>

  <?php echo form_close(); ?>

</div>
     