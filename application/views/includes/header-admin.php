<!DOCTYPE html> 
<html lang="en-US">
<head>
  <title>Admin Site</title>
  <meta charset="utf-8">
  <link href="<?php echo base_url(); ?>assets/css/admin/global.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar navbar-fixed-top">
	  <div class="navbar-inner">
	    <div class="container">
	      <a class="brand">Project Name</a>
	      <ul class="nav">
	       
	        <li <?php if($this->uri->segment(2) == 'categories'){echo 'class="active"';}?>>
	          <a href="<?php echo base_url(); ?>admin/categories">Category</a>
	        </li>
	        <li <?php if($this->uri->segment(2) == 'series'){echo 'class="active"';}?>>
	          <a href="<?php echo base_url(); ?>admin/series">Series</a>
	        </li>
	         <li <?php if($this->uri->segment(2) == 'questions'){echo 'class="active"';}?>>
	          <a href="<?php echo base_url(); ?>admin/questions">Question</a>
	        </li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">User <b class="caret"></b></a>
	          <ul class="dropdown-menu">
	            <li>
	              <a href="<?php echo base_url(); ?>admin/users">User</a>
	            </li>
	            <li>
	              <a href="<?php echo base_url(); ?>admin/users/winner">Winner</a>
	            </li>
	          </ul>
	        </li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">System <b class="caret"></b></a>
	          <ul class="dropdown-menu">
	            <li>
	              <a href="<?php echo base_url(); ?>admin/logout">Logout</a>
	            </li>
	          </ul>
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>	
