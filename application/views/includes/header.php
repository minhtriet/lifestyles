<!DOCTYPE html>
<html lang="en">
	
	<head>
	    <meta charset="utf-8" />
	    <title>LifeStyles</title>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    <meta name="apple-mobile-web-app-capable" content="yes" />
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" />
	    <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
	    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon" />
	    <!-- <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family='Myriad Pro'"> -->
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css" media="all"/>
	    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.bxslider.css" media="all"/>
    	<!-- Latest compiled and minified JavaScript -->
    	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js"></script>
		<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1587475418137530',
          xfbml      : true,
          version    : 'v2.1'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
      $(document).ready(function(){
		$('#fbsharebutton').click(function(e){
		e.preventDefault();
		FB.ui(
		{
			method: 'feed',
			link: '',
			picture: 'https://lusfacebookapp.silverlining.vn/assets/img/HVDY-fb-thumb.jpeg',
			caption: 'H.V.D.Y- Trường đào tạo kỹ năng “yêu” điêu luyện. ',
			description: 'Nâng cao kỹ năng “yêu” cho phái mạnh với hàng loạt tuyệt chiêu “hưng phấn” và “thăng hoa”, giúp người “nông dân” luôn có những cuộc “mây mưa” hoàn hảo và đáng nhớ!Tìm hiểu tại www.thuthachkienthucyeu.com  '
		});
		});
	});
    </script>
	</head>

<body>
	<div class="container">
