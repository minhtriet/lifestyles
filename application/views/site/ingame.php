<script>
  var popup=<?php echo $completed;?>,
      popup_err = false,
      isPlayedOut = <?php echo $totalPlayTimes; ?>,
      isPosted = <?php echo $isPosted; ?>,
      answeredCorrect = <?php echo $kq['answeredCorrect']; ?>;

  popup = ( popup == 0) ? true: false;
  isPlayedOut = ( isPlayedOut < 3 ) ? false : true;
  
  $(function() {
	    $( ".tabs" ).tabs({ show: { effect: "fadeIn", duration: 800 } });

        $( ".formstyle_register" ).dialog({
          autoOpen: popup,
          modal: true,
          show: {
            effect: "blind",
            duration: 800
          },
          hide: {
            effect: "explode",
            duration: 800
          }
        });
        $( ".formstyle_popup" ).dialog({
          autoOpen: false,
          modal: true,
          show: {
            effect: "blind",
            duration: 800
          },
          hide: {
            effect: "explode",
            duration: 800
          }
        });
        if(isPosted && answeredCorrect>=3){
          $('.ui-button').addClass("hide");
          $( ".form-info-congratulation" ).dialog( "open");
        }
        if(isPosted && answeredCorrect <3){
          $('.ui-button').addClass("hide");
          $( ".form-info-fail" ).dialog( "open");
        }
        if(isPlayedOut && !isPosted){
          $('.ui-button').addClass("hide");
          $( ".form-info-error" ).dialog( "open");
        }
        if(popup)
          $('.ui-button').addClass("hide");
        $( ".continue" ).click(function() {
            $('.ui-button').addClass("hide");
            // $( ".form-info" ).dialog( "open");  
            var question= $('.questions-active').next();
            // console.log(question.attr('class'));
            var valra=$('.questions-active').find('input[type="radio"]:checked').val();
            if(question.attr('class')=='questions')
            {
              
              if(typeof valra!='undefined')
              {
                $('.questions-active').addClass('questions').removeClass('questions-active');
                question.addClass('questions-active').removeClass('questions');
                question.fadeIn(3000);
              }
              else

                $('.frer-answer').dialog( "open");
            }
            else
              endgame();        
        });
        $('.close-alert').click(function(){
          $('.ui-button').addClass("hide");
          $( ".frer-answer" ).dialog( "close");      
        });
        $( ".closepopup" ).click(function() {
            $('.ui-button').addClass("hide");
            $( ".form-info" ).dialog( "close");         
        });
        $('.ic').click(function() {
            $('.ic-yellow').removeClass('ic-yellow');
            $(this).addClass('ic-yellow');
            $(this).find('input[type="radio"]').prop('checked', true);
          });
	    
  });
  
  var start = 90000;
  var intervel=setInterval(function() {
    if(start>=0 && !popup_err && !popup && !isPosted && !isPlayedOut)
    {
      $('#time-ingame').text(start / 1000 );
      $('#progress-time').width(Math.round((start*100/90000)*335/100));
      start-=1000;
    }
    else
    {
      if(popup_err || start<0 )
        endgame();
    }
}, 1000);
function endgame(){
      clearInterval(intervel);
      $( ".frer-answer" ).dialog( "close");
      $('#remainTime').val(start/1000);
      $('#form_ingame').submit();
     
}


</script>
	 <!-- Header -->
    <div class="header">
        <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>/assets/img/logo.png"></a>
        <div class="banner"></div>
        <a href="<?php echo base_url();?>thuthach" class="logo-hvdy"><img src="<?php echo base_url();?>/assets/img/hvdy.png"></a>
    </div>
    <!-- /. Header -->

	<!-- Main -->
	<div class="main">
		<div class="tabs">
			<div class="icon-girl">    				
			</div>
      <form method="post" action="<?php echo site_url('set-score')?>" id="form_ingame">
      <input type="hidden" name="remainTime" value="" id="remainTime">
			<div class="question-form">
				<div class="result">
					<div class="progress progress-striped active bar-timer">
                <div class="progress-bar" id="progress-time" style="width: 335px"></div>
          </div>
          <div class="time"><font size="4" id="time-ingame">90</font>s</div>
					<div class="tooltip"></div>
				</div>
        
        <?php if ( ! empty( $questions ) ) :
          $stt = array( 'A', 'B', 'C', 'D');
          $index = 1;
        ?>
            <?php foreach ($questions as $key => $question) : ?>
              <div class="<?php echo ($index == 1) ? 'questions-active' : 'questions' ?>">
        				<div class="question"><strong>Câu hỏi <?php echo $index++?>:</strong> <?php echo $question->question ?></div>
                <table class="question-detail">

                  <?php $i = 0;
                    foreach ($answers as $key => $answer ) :
                      if ( $question->id === $answer->questionId ) :
                  ?>
          					<tr>
                      <td>
                          <span class="ic" data="<?php echo $stt[$i];?>">
                            <label><input type="radio" value="<?php echo $answer->id?>" style="display:none" name="answers[<?php echo $question->id?>][]"><?php echo $stt[$i++];?>.
                          </span>
                      </td>
          						<td>
          						  <p><?php echo $answer->answer; ?></p>
                      </td>
          					</tr>
                  <?php 
                      endif;
                    endforeach; ?>
        					
        				</table>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
				<a class="continue" >Tiếp Tục</a>
			</div>
      <div class="form-info form-info-congratulation formstyle_popup" title="Basic dialog" style="display: none;">
                <h3 style="margin-left: 62px;">KẾT QUẢ</h3> 
                <div class="info-detail">
                    <div class="box-point">
                        <span>Điểm Kiến Thức</span>
                        <div class="icon-point"><?php echo $kq['knowledgePoints'] ?></div>
                    </div>      
                    <div class="box-point">
                        <span>Điểm Thưởng</span>
                        <div class="icon-point"><?php echo $kq['bonusPoints'] ?></div>
                    </div> 
                    <div class="box-point">
                        <span>Tổng Điểm</span>
                        <div class="icon-point icon-point-green"><?php echo $kq['knowledgePoints']+ $kq['bonusPoints']?></div>
                    </div>            
                    <a href="<?php echo base_url();?>chungnhan" class="close-alert">NHẬN GIẤY CHỨNG NHẬN</a>
                </div>
                <img src="<?php echo base_url();?>/assets/img/peolist1.png" class="congratulation">
            </div>
          <div class="form-info form-info-fail formstyle_popup" title="Basic dialog" style="display: none;">
              <h3 style="margin-top:15px;">KẾT QUẢ</h3> 
              <div class="info-detail">
                  <div class="box-point">
                      <span>Điểm Kiến Thức</span>
                      <div class="icon-point"><?php echo $kq['knowledgePoints'] ?></div>
                  </div>      
                  <div class="box-point">
                      <span>Điểm Thưởng</span>
                      <div class="icon-point"><?php echo $kq['bonusPoints'] ?></div>
                  </div> 
                  <div class="box-point">
                      <span>Tổng Điểm</span>
                      <div class="icon-point icon-point-green"><?php echo $kq['knowledgePoints']+ $kq['bonusPoints']?></div>
                  </div>            
                  <div class="message">Rất tiếc! Bạn không đủ điều kiện vượt qua thử thách.</div>         
                  <a href="<?php echo base_url();?>ingame" class="close-alert" style="margin-left:45px;">CHƠI LẠI</a>
              </div>
              <!-- <img src="<?php echo base_url();?>images/congratulation.png" class="congratulation"> -->
          </div>
      </form>
		</div>
		<div class="menu">
			<ul>
				<li><a href="<?php echo base_url();?>video">Các Khóa Học <br> H.V.D.Y</a></li>
				<li><a href="<?php echo base_url();?>tophocvien">Top Học Viện<br> Xuất Sắc</a></li>
				<li class="bonus"> <img src="<?php echo base_url();?>/assets/img/bonus.png"><a href="<?php echo base_url();?>giaithuong">Giải <br> Thưởng</a></li>
				<li><a href="<?php echo base_url();?>thele">Thể Lệ<br> Chương Trình</a></li>
			</ul>
			<a href="#" class="share-face" id="fbsharebutton"></a>
		</div>
		<div class="form-info registerform formstyle_register" title="Basic dialog" style="display:none;">
          <form method="post" action="<?php echo site_url('player/update')?>" >
            <h3>THÔNG TIN CÁ NHÂN</h3> 
            <div class="info-detail">
                <div class="info-detail-col">
                   <div class="detail-title">Ngày Sinh:</div>
                   <select name="dob[bday]">
                      <?php for ($day=1; $day < 32; $day++ ) { 
                       echo "<option value='$day'>$day</option>";
                      }?>
                   </select>
                   <select name="dob[bmonth]">
                      <?php for ($month=1; $month < 13; $month++ ) { 
                       echo "<option value='$month'>$month</option>";
                      }?>
                   </select>
                   <select name="dob[byear]">
                      <?php for ($year=1975; $year < date('Y'); $year++ ) { 
                       echo "<option value='$year'>$year</option>";
                      }?>
                   </select>
                </div>
                <div class="info-detail-col">
                   <div class="detail-title">Số Điện Thoại:</div>
                   <input type="text" name="mobifone" value="0984046223">
                </div>
                <button type="submit" class="complete closepopup">HOÀN TẤT</button>
            </div>
          </form>

        </div>
        <!-- error chua chon cau hoi -->
        <div class="form-info frer-answer formstyle_popup" title="Basic dialog" style="display: none;">
                <h3>BÁO LỖI</h3> 
                <div class="info-detail">
                   <p style="text-align: center;">Bạn chưa chọn câu trả lời.<br>
                    Vui lòng chọn để tiếp tục! 
                    </p>                      
                    <a class="close-alert" style="margin-left:43px;">ĐÓNG THÔNG BÁO</a>
                </div>

            </div>
            <!-- het luot choi  -->
            <div class="form-info form-info-error formstyle_popup" title="Basic dialog" style="display: none;">
                <h3>HẾT LƯỢT CHƠI!</h3> 
                <div class="info-detail" style="width: 330px;">
                   <p>Bạn đã sử dụng hết 3 lượt chơi <br>
của ngày hôm nay. 
                    </p>                      
                    <a href="<?php echo base_url();?>site" class="close-alert" style="margin-left:43px;">VỀ TRANG CHỦ</a>
                </div>

	</div>
	<!-- /. Main -->