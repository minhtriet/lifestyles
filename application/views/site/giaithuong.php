<script>
  $(function() {
    $( ".tabs" ).tabs({ show: { effect: "fadeIn", duration: 800 } });
  });
  </script>

	<!-- Header -->
    <div class="header">
        <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>/assets/img/logo.png"></a>
        <div class="banner"></div>
        <a href="<?php echo base_url();?>thuthach" class="logo-hvdy"><img src="<?php echo base_url();?>/assets/img/hvdy.png"></a>
    </div>
	<!-- /. Header -->

	<!-- Main -->
	<div class="main">
		<div class="tabs">
			<ul>
			    <li><a href="#tabs-1" class="tab-giaithuong">GIẢI THƯỞNG</a></li>
			    <li><a href="#tabs-2" class="tab-thele">THỂ LỆ</a></li>				   
			</ul>
			<div id="tabs-1">
		   		<div class="tabs-item" style="margin-top: 0px;">
					<div class="widget-list-sanpham-left-333" style="margin-left: 35px;">
							<img class="finaltop-10-thanghoa" src="<?php echo base_url();?>/assets/img/thanghoacuoikhoa.png">
							<div class="title-about-1">1 Voucher CGV 400k <br>
											+ 1 voucher nhà hàng Monsoon 700k<br>
											+ 1 Hộp B.C.S LifeStyles
							</div>
					</div>
					<div class="widget-list-sanpham-right-333" style="margin-right: 55px;margin-top: 12px;">
						
						<p class="content-about-333">
							<img class="finaltop-10" src="<?php echo base_url();?>/assets/img/giaihungphanmoingay.png">
						</p>
						<div class="title-about-1">1 Cặp Voucher CGV 200k<br>
											+ 1 Hộp B.C.S LifeStyles
						</div>
					</div>
			   </div>
				<a href="<?php echo base_url();?>ingame">
            				<div class="btn-thty" style="margin-top: -15px;"></div>
        		</a>
			</div>
			<div id="tabs-2">
			   <div class="tabs-item">
			   		<div class="widget-list-sanpham-left-1" style="margin-left:40px;">
						<div class="scrollbar-222" id="style-5">
							  <div class="force-overflow-1 title-6-2-the">
							 <p style="text-align: center;font-size:22px;">
	<strong>Thể lệ chương trình</strong>
</p>
<p style="text-align: center;font-size:26px;padding-top: 0px;padding-bottom: 10px;">
	<strong>THỬ THÁCH KIẾN THỨC "YÊU"</strong>
</p>
<p>
	<b>1.</b><b> Tên chương trình:</b> Thử Thách Kiến Thức "Yêu" - Challenge Your Sextraordinary Knowledge
</p>
<p>
	<b>2.</b><b> Phạm vi chương trình: Toàn quốc</b>
</p>
<p>
	<b> Thời gian:  </b>Đợt 1: Từ 21/03/2015 đến 31/03/2015
</p>
<p style="margin-left: 56px;">
	 Đợt 2: Từ 04/04/2015 đến 14/04/2015
</p>
<p>
	<b>3.</b><b> Thông tin: </b>Là một hoạt động nằm trong chiến dịch <b>Học Viện Dạy "Yêu" </b>của thương hiệu bao cao su LifeStyles, cuộc thi <b>Thử Thách Kiến Thức "Yêu"</b> thử thách kiến thức và kỹ năng "xếp hình" của cánh mày râu dựa trên kiến thức được cung cấp từ các khóa học của Học Viện Dạy "Yêu". Người chơi có điểm số cao sẽ có cơ hội đạt được các giải thưởng hấp dẫn của chương trình.
</p>
<p>
	<b>4.</b><b> </b><b>Người tham gia:</b>
</p>
<p>
	-  Công dân Việt Nam trên 18 tuổi, thành viên của trang Facebook LifeStyles, sinh sống và làm việc hoặc học tập tại Việt Nam.
</p>
<p>
	-  Có tài khoản Facebook đang hoạt động, số CMND và số điện thoại để BTC liên lạc và xác nhận khi trao giải hoặc phục vụ các công tác hành chính.
</p>
<p>
	<b>5.</b><b> Giải thưởng:</b>
</p>
<p>
	Có 2 bài trắc nghiệm tương đương 2 đợt thi xuyên suốt chương trình. Mỗi đợt thi diễn ra trong 11 ngày. Các giải thưởng bên dưới sẽ được áp dụng cho mỗi đợt.
</p>
<p>
	<b>5.1 </b><b>Giải thưởng hàng ngày</b>
</p>
<ul>
	<li>Giải thưởng hàng ngày được áp dụng trong vòng 10 ngày diễn ra đợt thi 1 từ 21/03/2015 đến 30/03/2015 và đợt thi 2 từ 04/04/2015 đến 13/04/2015.</li>
	<li>Mỗi ngày, 10 người chơi đạt điểm số cao nhất sẽ nhận giải thưởng hàng ngày.</li>
	<li>Giải thưởng bao gồm 1 hộp bao cao su LifeStyles + 1 voucher vé xem phim CGV trị giá 200.000 VNĐ.</li>
</ul>
<p>
	<b>5.2 </b><b>Giải thưởng chung cuộc</b>
</p>
<ul>
	<li>Thời gian chọn ra người chiến thắng chung cuộc sẽ kết thúc lúc 5 giờ chiều ngày 30/03/2015 cho đợt thi 1 và ngày 13/04/2015 cho đợt thi 2.</li>
	<li>Sau 11 ngày diễn ra chương trình, 5 người chơi có điểm số cao nhất sẽ nhận giải thưởng chung cuộc. </li>
	<li>Giải thưởng là 1 phần thưởng lãng mạn trọn gói cho 2 người bao gồm 1 voucher ăn tối tại nhà hàng trị giá 700.000VNĐ, 1 voucher vé xem phim CGV trị giá 400.000VNĐ và 1 hộp bao cao su LifeStyles.</li>
</ul>
<p>
	- 
	<b>Tổng số giải thưởng là 200 giải hàng ngày và 10 giải chung cuộc.</b>
</p>
<p>
	<b>6. Cách tham gia cuộc thi</b>
</p>
<p style="margin-left: 20px;">
	- <u>Bước 1:</u> Click chọn mục <b>Thử Thách Kiến Thức "Yêu"</b> trên trang Facebook của LifeStyles tại <a href="http://www.facebook.com/LifeStylesVietnam">www.facebook.com/LifeStylesVietnam</a> . Những người không phải là thành viên của trang fanpage, click "Like" để tham gia.
</p>
<p style="margin-left: 20px;">
	- <u>Bước 2:</u> Nhập thông tin cá nhân và tham gia các khóa học của Học Viện Dạy "Yêu", tìm hiểu kiến thức "xếp hình", hoặc bỏ qua bước này nếu bạn đã xem rồi.
</p>
<p style="margin-left: 20px;">
	- <u>Bước 3:</u> Trả lời 5 câu hỏi trắc nghiệm trong thời gian 90 giây. Mỗi câu trả lời đúng sẽ nhận 10 điểm. Nếu người chơi hoàn thành bài kiểm tra trước thời gian quy định, mỗi giây còn lại sẽ được tính 1 điểm.
</p>
<p style="margin-left: 20px;">
	<i><u data-redactor-tag="u">Ghi chú:</u></i> Mỗi tài khoản/ người chơi chỉ được 3 lượt chơi mỗi ngày.
</p>
<p style="margin-left: 20px;">
	- <u>Bước 4:</u> Sau khi hoàn thành bài kiểm tra, người chơi sẽ được nhận Chứng Nhận Tốt Nghiệp. Có 2 loại chứng nhận tùy vào điểm số của người chơi:
</p>
<ul>
	<ul>
		<li>Chứng nhận loại xuất sắc: dành cho người có 5 câu trả lời đúng.</li>
		<li>Chứng nhận loại giỏi: dành cho những người chỉ trả lời đúng 3 hoặc 4 câu.</li>
	</ul>
</ul>
<p style="margin-left: 20px;">
	<i><u data-redactor-tag="u">Ghi chú:</u></i> Nếu người chơi có dưới 3 câu trả lời đúng, sẽ không được công nhận là đã vượt qua bài trắc nghiệm và không được nhận giấy chứng nhận.
</p>
<p style="margin-left: 20px;">
	<u>Bước 5:</u> 10 người chơi có số điểm cao nhất trong ngày sẽ đạt được giải thưởng hàng ngày
</p>
<p style="margin-left: 20px;">
	<u>Ghi chú:</u> Cuộc thi hàng ngày sẽ bắt đầu lúc 00:01 sáng đến 11:59 tối cùng ngày và kết quả sẽ được công bố vào ngày tiếp theo.
</p>
<p style="margin-left: 20px;">
	- <u>Bước 6:</u> 5 người có điểm số cao nhất của mỗi đợt thi sẽ đạt giải thưởng chung cuộc.
</p>
<p style="margin-left: 20px;">
	- <i><u data-redactor-tag="u">Ghi chú:</u></i>
</p>
<ul>
	<ul>
		<li>Người chơi có thể làm lại bài trắc nghiệm nhiều lần để vào Top 5 người có điểm cao nhất. Danh sách Top 5 sẽ được công bố vào cuối mỗi đợt thi. </li>
		<li>Tất cả điểm số và kết quả thi chỉ được sử dụng cho mỗi đợt thi tương ứng. Khi đợt thi 2 diễn ra, toàn bộ điểm số của đợt 1 sẽ bị xóa. Do đó, người chơi sẽ phải bắt đầu lại từ đầu.</li>
		<li>Trong trường hợp có nhiều người chơi đồng điểm, BTC sẽ tiến hành chọn ngẫu nhiên người thắng giải cuối cùng.</li>
	</ul>
</ul>
<p>
	<b>7. Thời gian công bố kết quả</b>
</p>
<p>
	Các giải thưởng hàng ngày và giải thưởng chung cuộc sẽ được thông báo trên trang Facebook của LifeStyles tại 
	<a href="http://www.facebook.com/LifeStylesVietnam">www.facebook.com/LifeStylesVietnam</a>
</p>
<p>
	<b>8. Các quy định về giải thưởng</b>
</p>
<p>
	-  Các giải thưởng sẽ được trao cho người chiến thắng trong vòng 30 ngày sau khi cuộc thi kết thúc. BTC sẽ liên lạc với người chiến thắng qua thông tin cá nhân người chơi dùng để đăng ký khi tham gia cuộc thi. BTC sẽ không chịu trách nhiệm về bất kì mất mát trong quá trình trao giải do lỗi của người chơi (ví dụ như: cung cấp sai thông tin cá nhân).
</p>
<p>
	-  Những người chiến thắng phải nộp bản photo CMND hoặc giấy tờ có liên quan để BTC xác nhận và đối chiếu khi trao giải.
</p>
<p>
	-  Trong vòng 10 ngày làm việc sau khi thông báo kết quả, nếu BTC không liên lạc được, người chiến thắng sẽ được xem như từ chối nhận giải. Và giải thưởng sẽ được trao cho người có điểm số cao tiếp theo. Người chiến thắng sẽ không có quyền khiếu nại, kiện tụng hoặc yêu cầu bồi thường trong trường hợp này. BTC có đầy đủ thẩm quyền để xử lý phần thưởng theo luật pháp.
</p>
<p>
	<b>9. Quy định chung</b>
</p>
<p>
	- BTC có đầy đủ thẩm quyền loại bỏ bất kì nội dung mà BTC cho là nhạy cảm hoặc làm ảnh hưởng đến thuần phong mỹ tục Việt Nam.
</p>
<p>
	- Người chơi sẽ chịu trách nhiệm về bất cứ tranh chấp, khiếu nại, kiện tụng, bồi thường liên quan đến bản quyền, quyền sở hữu của các câu trả lời/ phát biểu mà người chơi đưa ra trong cuộc thi.
</p>
<p>
	- BTC sẽ không chịu trách nhiệm và không có nghĩa vụ giải quyết bất kì khiếu nại hoặc tranh chấp liên quan đến quyền sở hữu của các câu trả lời mà người chơi đưa ra trong cuộc thi.
</p>
<p>
	- Người tham gia nên đọc và hiểu rõ các yêu cầu, chấp thuận và làm theo các quy định của cuộc thi. Họ phải chắc chắn có đầy đủ quyền sử dụng hợp pháp các thông tin đăng ký. LifeStyles sẽ không chịu trách nhiệm về bất kì tranh chấp về bản quyền.
</p>
<p>
	- BTC sẽ công bố danh sách người trúng thưởng tại trang 
	<a href="http://www.facebook.com/LifeStylesVietnam">www.facebook.com/LifeStylesVietnam</a> và có quyền sử dụng clip, thông tin của người tham gia mà không phải trả thêm bất kì chi phí nào.
</p>
<p>
	- Người chiến thắng sẽ phải tuân thủ thời gian và sự sắp xếp của BTC trong quá trình trao giải.
</p>
<p>
	- BTC có quyền hủy quyền tham gia cuộc thi của người chơi tại 
	<a href="http://www.facebook.com/LifeStylesVietnam">www.facebook.com/LifeStylesVietnam</a> mà không cần phải thông báo trước nếu BTC phát hiện ra bất kì hành động gian lận nào.
</p>
<p>
	- BTC sẽ thông báo nếu có bất kì thay đổi nào về cuộc thi trong suốt quá trình tổ chức tại 
	<a href="http://www.facebook.com/LifeStylesVietnam">www.facebook.com/LifeStylesVietnam</a>. Trong trường hợp khách quan, bất khả kháng (thiên tai, lũ lụt, đình công, khủng bố làm mất dữ liệu thông tin đăng ký của thí sinh), BTC có quyền thay đổi hoặc hủy bỏ cuộc thi và gửi thông báo cho người chơi trong thời gian sớm nhất.
</p>
<p>
	- Trong trường hợp phát sinh tranh chấp, khiếu nại sẽ được giải quyết trực tiếp và quyết định của BTC là quyết định cuối cùng.
</p>
<p>
	- Nhân viên của LifeStyles Việt Nam và các đối tác sẽ không được tham gia vào cuộc thi này.
</p>
<p>
	BTC
</p>
<p>
	LifeStyles Vietnam
</p>
							  </div>
						</div>
						<a href="<?php echo base_url();?>ingame">
            				<div class="btn-thty" ></div>
        				</a>
					</div>
				
			   </div>
			</div>
			
		</div>
		<div class="menu">
			<ul>
				<li><a href="<?php echo base_url();?>video">Các Khóa Học <br> H.V.D.Y</a></li>
				<li><a href="<?php echo base_url();?>tophocvien">Top Học Viện<br> Xuất Sắc</a></li>
				<li class="bonus buttom-actives-111"> <img src="<?php echo base_url();?>/assets/img/bonus.png"><a href="javascript:void(0)">Giải <br> Thưởng</a></li>
				<li><a href="<?php echo base_url();?>thele">Thể Lệ<br> Chương Trình</a></li>
			</ul>
			<a href="#" class="share-face" id="fbsharebutton"></a>
		</div>
	</div>
	<!-- /. Main -->