
<script>
  $(function() {
	    $( ".tabs" ).tabs({ show: { effect: "fadeIn", duration: 800 } });

	    $( "#product-detail-1111" ).dialog({
	      autoOpen: false,
	      modal: true,
	      show: {
	        effect: "blind",
	        duration: 800
	      },
	      hide: {
	        effect: "explode",
	        duration: 800
	      }
	    });
	 
	    $( ".box a" ).click(function() {
    	var myposition={ my: "center center", at: "center top+340", of: ".container"};
		$( "#product-detail-1111" ).dialog({position:myposition});	 
	      $( "#product-detail-1111" ).dialog( "open");	      
	    });

	    $('.bxslider').bxSlider({
				mode: 'fade',
				speed : 700
		 });
  });
</script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.0.14/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript">
			$(function()
			{
				$('#section').jScrollPane();
			});
			
		</script> 

  		<!-- Header -->
	    <div class="header">
	        <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>/assets/img/logo.png"></a>
	        <div class="banner"></div>
	        <a href="<?php echo base_url();?>thuthach" class="logo-hvdy"><img src="<?php echo base_url();?>/assets/img/hvdy.png"></a>
	    </div>
	    <!-- /. Header -->

    	<!-- Main -->
    	<div class="main">
    		<div class="tabs">
    			<ul>
				    <li><a href="#tabs-1" class="tab-top5">TOP 5 TRANH GIẢI CHUNG CUỘC</a></li>
				    <li><a href="#tabs-2">TOP 10 GIẬT GIẢI MỖI NGÀY</a></li>				   
				</ul>
				<div id="tabs-1">
			   		<div class="tabs-item" style="margin-top:8px">
						<div class="widget-list-sanpham-left-222">
							<div class="scrollbar" id="style-5">
								  <div class="force-overflow">
									<table class="table-top">
										<tr class="border-top-tr" >
											<td class="border-top-td-stt" >STT</td>
											<td class="border-top-td-ten" style="text-align:center !important">TÊN</td>
											<td class="border-top-td-diem">ĐIỂM</td>
										</tr>

										<?php if ( ! empty( $top5 ) ) :
											$index = 1;
										?>
											<?php foreach ( $top5 as $key => $player ) :
											?>
												<tr class="border-top-tr">
													<td class="border-top-td-stt"><div class="border-top-td-stt-bg"><?php echo $index++; ?></div></td>
													<td class="border-top-td-ten"><img style="margin:0px 10px 0px" src="<?php echo $player->avatar; ?>" height="27"><?php echo $player->name; ?></td>
													<td class="border-top-td-diem"><?php echo $player->score; ?></td>
												</tr>
										<?php endforeach; ?>
										<?php endif; ?>
										
									</table>
								  </div>
							</div>
						</div>
						<div class="widget-list-sanpham-right-222" >
							
							<p class="content-about-222 finaltop-10" style="padding:23px 13px 0px 0px;">
								<a href="<?php echo base_url();?>giaithuong"><img src="<?php echo base_url();?>/assets/img/TOP-FINALTOP-10.png"></a>
							</p>
							<div class="title-about title-tophocvien">GIẢI "THĂNG HOA" CUỐI KHÓA</div>
						</div>
				   </div>
				</div>
				<div id="tabs-2">
				   <div class="tabs-item" style="margin-top:8px;">
				   		<div class="widget-list-sanpham-left-222">
							<div class="scrollbar" id="style-5">
								  <div class="force-overflow">
									<table class="table-top">
										<tr class="border-top-tr" >
											<td class="border-top-td-stt" >STT</td>
											<td class="border-top-td-ten" style="text-align:center !important">TÊN</td>
											<td class="border-top-td-diem">ĐIỂM</td>
										</tr>

										<?php if ( ! empty( $top10 ) ) :
											$index = 1;
										?>
											<?php foreach ( $top10 as $key => $player ) :
											?>
												<tr class="border-top-tr">
													<td class="border-top-td-stt"><div class="border-top-td-stt-bg"><?php echo $index++; ?></div></td>
													<td class="border-top-td-ten"><img style="margin:0px 10px 0px;" src="<?php echo $player->avatar; ?>" height="27"><?php echo $player->name; ?></td>
													<td class="border-top-td-diem"><?php echo $player->score; ?></td>
												</tr>
										<?php endforeach; ?>
										<?php endif; ?>
									</table>
								  </div>
							</div>
						</div>
					
						
						<!--------########################------->
						<div class="widget-list-sanpham-right-222">
							
							<p class="content-about-222 finaltop-10" style="padding:0 5px;">
								<a href="<?php echo base_url();?>giaithuong"><img src="<?php echo base_url();?>/assets/img/TOP-DAILYtop10.png"></a>
							</p>
							<div class="title-about title-tophocvien">GIẢI "HƯNG PHẤN" MỖI NGÀY</div>
				
							<div class="box">
								<a href="#">
									 <div class="button-danh-sach-giai" ></div>
								</a>
							</div>

							<div id="product-detail-1111" title="Basic dialog" style="display: none;">
							<div class="title-about-22222">DANH SÁCH HỌC VIÊN THẮNG GIẢI NGÀY</div>
								<div class="scrollbar" id="style-5" style="height: 203px;">
								  <div class="force-overflow" style="height: 450px;">
									<table class="table-top">
										<tr class="border-top-tr" >
											<td class="border-top-td-stt" >STT</td>
											<td class="border-top-td-ten" style="text-align:center !important">TÊN</td>
											<td class="border-top-td-diem">ĐIỂM</td>
										</tr>

										<?php if ( ! empty( $winersDay ) ) :
											$index = 1;
										?>
											<?php foreach ( $winersDay as $key => $player ) :
											?>
												<tr class="border-top-tr">
													<td class="border-top-td-stt"><div class="border-top-td-stt-bg"><?php echo $index++; ?></div></td>
													<td class="border-top-td-ten"><img style="margin:0px 10px 0px ;" src="<?php echo $player->avatar; ?>" height="27"><?php echo $player->name; ?></td>
													<td class="border-top-td-diem"><?php echo $player->score; ?></td>
												</tr>
										<?php endforeach; ?>
										<?php endif; ?>
										
							
										
									</table>
								  </div>
							</div>
							</div>
						</div>
						<!--------########################------->
						
						
				   </div>
				</div>
				
    		</div>
    		<div class="menu">
    			<ul>
    				<li><a href="<?php echo base_url();?>video">Các Khóa Học <br> H.V.D.Y</a></li>
    				<li class="buttom-actives-111"><a href="javascript:void(0)">Top Học Viện<br> Xuất Sắc</a></li>
    				<li class="bonus"> <img src="<?php echo base_url();?>/assets/img/bonus.png"><a href="<?php echo base_url();?>giaithuong">Giải <br> Thưởng</a></li>
    				<li><a href="<?php echo base_url();?>thele">Thể Lệ<br> Chương Trình</a></li>
    			</ul>
    			<a href="#" class="share-face" id="fbsharebutton"></a>
    		</div>
    	</div>
    	<!-- /. Main -->

  </div>
