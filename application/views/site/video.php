<?php
    $this->load->config( 'video' );
    $videoId1 = $this->config->item( 'videoId1' );
    $videoId2 = $this->config->item( 'videoId2' );
    $videoId3 = $this->config->item( 'videoId3' );
?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.youtubepopup.min.js"></script>
<script type="text/javascript">
        $(function () {
            

            $("a.youtube").YouTubePopup({ autoplay: 0, width: 591, height: 427, idAttribute: 'videoPopup' });
            $("#<?php echo $videoId1; ?>").YouTubePopup({ youtubeId: "<?php echo $videoId1; ?>", width: 591, height: 427 });
            $("#<?php echo $videoId2; ?>").YouTubePopup({ youtubeId: "<?php echo $videoId2; ?>", width: 591, height: 427 });
            $("#<?php echo $videoId3; ?>").YouTubePopup({ youtubeId: "<?php echo $videoId3; ?>", width: 591, height: 427 });
            
            var main = $('.main-left-about'),
                left = main.position().left;

            $("a.youtube, #<?php echo $videoId1; ?>, #<?php echo $videoId2; ?>, #<?php echo $videoId3; ?>").click( function() {
                $('.YouTubeDialog').css( 'top', '-502px' ).css( 'left', ( left + 43 ) + 'px' );
                $('.ui-dialog .ui-button-text').css('right', -114 + 'px');
            });
        });
    </script>
    <!-- Header -->
    <div class="header">
        <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>/assets/img/logo.png"></a>
        <div class="banner"></div>
        <a href="<?php echo base_url();?>thuthach" class="logo-hvdy"><img src="<?php echo base_url();?>/assets/img/hvdy.png"></a>
    </div>
    <!-- /. Header -->

    <div id="about">
       
    <div class="main-left-about">
        <div class="title">CÁC KHÓA HỌC "YÊU"</div>
        <div class="view">
            <ul>
                <li>
                    <img class="youtube" id="<?php echo $videoId1; ?>" src="http://img.youtube.com/vi/<?php echo $videoId1; ?>/default.jpg" title="Mắt Nai" />
                    <p>
                        <a class="youtube" href="http://www.youtube.com/watch?v=<?php echo $videoId1; ?>" title="Mắt Nai">Mắt Nai</a>
                    </p>
                </li>
                <li>
                    <img class="youtube" id="<?php echo $videoId2; ?>" src="http://img.youtube.com/vi/<?php echo $videoId2; ?>/default.jpg" title="Đa Tình" />
                    <p>
                        <a class="youtube" href="http://www.youtube.com/watch?v=<?php echo $videoId2; ?>" title="Đa Tình">Đa Tình</a>
                    </p>
                </li>
                <li>
                    <img class="youtube" id="<?php echo $videoId3; ?>" src="http://img.youtube.com/vi/<?php echo $videoId3; ?>/default.jpg" title="Kiêu Kỳ" />
                    <p>
                        <a class="youtube" href="http://www.youtube.com/watch?v=<?php echo $videoId3; ?>" title="Mắt Nai">Kiêu Kỳ</a>
                    </p>
                </li>

            </ul>
        </div>
        <a href="<?php echo base_url();?>ingame">
            <div class="buttom-video" ></div>
        </a>
    </div>
    <div class="main-right-111">
        <div class="menu" style="margin-top: -265px;">
            <ul>
                <li class="buttom-actives-111"><a href="javascript:void(0)">CÁC KHÓA HỌC <br> H.V.D.Y</a>
                </li>
                <li><a href="<?php echo base_url();?>tophocvien">TOP HỌC VIÊN<br> XUẤT SẮC</a>
                </li>
                <li class="bonus">
                    <img src="<?php echo base_url();?>/assets/img/bonus.png"><a href="/giaithuong">GIẢI <br> THƯỞNG</a>
                </li>
                <li><a href="<?php echo base_url();?>thele">THỂ LỆ<br> CHƯƠNG TRÌNH</a>
                </li>
            </ul>
            <a href="#" class="share-face" id="fbsharebutton"></a>
        </div>

    </div>
</div>
