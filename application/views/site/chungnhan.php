<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/html2canvas.min.js"></script>
<script>
	var name='<?php echo $name;?>';
	var xeploai='<?php echo $xeploai;?>';
	$(document).ready(function(){
		$('#result_share').click(function(e){
		e.preventDefault();
		html2canvas($('#target'), {
            onrendered: function(canvas) {
                $('#img_val').val(canvas.toDataURL("image/png"));
                $.ajax({
                    url: $('#myForm').attr('action'),
                            type: 'POST',
                            data: $('#myForm').serialize(),
                            dataType: "json",
                    })
                    .done(function(msg) {
                    	console.log(msg.img);
                            FB.ui(
								{
									method: 'feed',
									name: name,
									link: 'https://www.facebook.com/cuachangVN/app_785219918183716?sk=app_785219918183716&ref=ts',
									picture: msg.img,
									caption: 'H.V.D.Y- Trường đào tạo kỹ năng “yêu” điêu luyện. ',
									description: 'Đã đạt trình yêu lão luyện cấp độ '+ xeploai +'. Bạn có tự tin với kiến thức yêu yêu luyện của mình không? Thử thách trình yêu ngay tại Học Viện Dạy Yêu Của LifeStyles.',
									message: ''
								});
                        })	            
            	}
        	});
		
		});
		
	});
	function Print(){
			// html2canvas($('#target'), {
   //          onrendered: function(canvas) {
   //              var url=canvas.toDataURL();
   //              	$('#print').attr('href', url);
   //              	document.getElementById('print').click(); 	            
   //          	}
   //      	});
			w=window.open();
			w.document.write($('#target').html());
			w.print();
			w.close();
		};
</script>
<a href="" download="chungnhan.png" id="print" style="display:none" ></a>
<form method="POST" enctype="multipart/form-data" action="<?php echo base_url()?>chungnhan/print" id="myForm">
    <input type="hidden" name="img_val" id="img_val" value="" />
</form>
<div id="fb-root"></div>
<!-- <div style="height:20px"><a href="" id="download">Download as image</a></div> -->
<div id="about">
		<div class="main-left-excell">	
			<div class="certification <?php echo ($xeploai == 'Giỏi') ? 'good-cer' : 'excel-cer' ?> " id="target">
				
				<?php if($xeploai == 'Giỏi'): ?>
					<div class="cer-logo11">
					<a href="<?php echo base_url();?>"><div class="circular_gioi" style="background:url(<?php echo $avatar ?>) no-repeat center center;"></div></a>
				<?php else : ?>
					<div class="cer-logo">
					<a href="<?php echo base_url();?>"><div class="circular" style="background:url(<?php echo $avatar ?>) no-repeat center center;"></div></a>
				<?php endif; ?>

				</div>
				<div class="cer-content">
				<div class="cer-desc">
				<p class="upper-text"><span class="cer-name"><?php echo $name ?></span></p>
				</div>
				<div class="cer-display" style="position: relative;right: 180px;top: 15px;">
				<p>Đã đạt trình "yêu" lão luyện cấp độ <span class="upper-text"><?php echo $xeploai; ?> </span>, có khả năng gây hưng phấn cao </p>
				<p>và làm người tình "sướng" tê tái trong mọi trận "xếp hình".</p>
				</div>
				</div>
				<div class="cer-note">
				<p>Lưu ý: Đừng quên dùng B.C.S LifeStyles để bảo vệ bản thân và tận hưởng chuyện ấy theo cách của bạn.</p>
				</div>
			</div>	
				<div class="button-excell"><a href="<?php echo base_url();?>ingame">THỬ THÁCH TRÌNH "YÊU"</a></div>
				<div class="button-excell-chiase" id="result_share"><a href="#">CHIA SẺ</a></div>
				<div class="button-excell-in" onclick="Print()"><a >IN</a></div>
			</div>
		
		<div class="main-right">
			<div class="button-1">
				<a href="<?php echo base_url();?>video">CÁC KHÓA HỌC <br> H.V.D.Y</a>
			</div>
			<div class="button-1">
				<a href="<?php echo base_url();?>tophocvien">TOP HỌC VIÊN <br>XUẤT SẮC</a>
			</div>
			<div class="button-gt">
				<a href="<?php echo base_url();?>giaithuong"><img src="<?php echo base_url();?>/assets/img/ABOUTgt.png"></a>
			</div>
			<div class="button-1">
				<a href="<?php echo base_url();?>thele">THỂ LỆ<br> CHƯƠNG TRÌNH</a>
			</div>
			<div class="facebook-about" id="fbsharebutton">
				<a href="#"><img src="<?php echo base_url();?>/assets/img/facebook.png"></a>
			</div>
			
		</div>
	</div>
