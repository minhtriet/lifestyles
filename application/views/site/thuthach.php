<script>
  $(function() {
	    $( ".tabs" ).tabs({ show: { effect: "fadeIn", duration: 800 } });

	    $( "#product-detail" ).dialog({
	      autoOpen: false,
	      modal: true,
	      show: {
	        effect: "blind",
	        duration: 800
	      },
	      hide: {
	        effect: "explode",
	        duration: 800
	      }
	    });
	 
	    $( ".box" ).click(function() {
	  		var index = $(this).attr('data');
	      var myposition={ my: "center center", at: "center center", of: ".container"};
			$( "#product-detail" ).dialog({position:myposition});	      
			$( "#product-detail" ).dialog( "open");
			slider.goToSlide(index);
	    });

	    var slider = $('.bxslider').bxSlider({
				mode: 'fade',
				speed : 700
		 });
  });
</script>
<!-- Header -->
    <div class="header">
        <a href="<?php echo base_url();?>" class="logo"><img src="<?php echo base_url();?>/assets/img/logo.png"></a>
        <div class="banner"></div>
        <a href="<?php echo base_url();?>thuthach" class="logo-hvdy"><img src="<?php echo base_url();?>/assets/img/hvdy.png"></a>
    </div>
<!-- /. Header -->

<!-- Main -->
<div class="main">
	<div class="tabs">
		<ul>
		    <li><a href="#tabs-1" class="hvdy">H.V.D.Y</a></li>
		    <li><a href="#tabs-2" class="sanpham">SẢN PHẨM</a></li>				   
		</ul>
		<div id="tabs-1">
			<div class="tabs-item">
				<div class="widget-list-sanpham-left">
					<img src="<?php echo base_url();?>/assets/img/ABOUT-nguoi.png">
				</div>
				<div class="widget-list-sanpham-right">
					<div class="title-about">TRƯỜNG ĐÀO TẠO<BR> KỸ NĂNG "YÊU" ĐIÊU LUYỆN</div>
					<p class="content-about">
					Nâng cao kỹ năng “yêu”, bổ trợ kiến thức “giường trường” cho <br>phe mày râu, giúp các chàng luôn có những cuộc “mây mưa” <br> hoàn hảo và đáng nhớ.
						<br><br>
					Tìm hiểu về học viện tại www.hocviendayyeu.com
					</p>
				</div>
			</div>     
			<a href="<?php echo base_url();?>ingame">
            	<div class="btn-thty" ></div>
        	</a>
		</div>

		<div id="tabs-2">
		   <div class="tabs-item">
		   		<div class="box" data="0">
		   			<a href="#"  class="img-product-small">
			   			<img src="<?php echo base_url();?>/assets/img/product-1.png">
			   			<span>Classic</span>
		   			</a>
		   		</div>
		   		<div class="box" data="1">
		   			<a href="#"  class="img-product-small">
			   			<img src="<?php echo base_url();?>/assets/img/product-2.png">
			   			<span>Fantasy </span>
		   			</a>
		   		</div>
		   		<div class="box" data="2">
		   			<a href="#"  class="img-product-small">
		   				<img src="<?php echo base_url();?>/assets/img/product-3.png">
		   				<span>Strawberry</span>
		   			</a>
		   		</div>
		   		<div class="box" data="3">				   			
		   			<a href="#"  class="img-product-small">
		   				<img src="<?php echo base_url();?>/assets/img/product-4.png">
		   				<span>LoveTime</span>
		   			</a>
		   		</div>
		   		<div class="box" data="4">
		   			<a href="#"  class="img-product-small">
			   			<img src="<?php echo base_url();?>/assets/img/product-5.png">
			   			<span>Sensation</span>
			   		</a>
		   		</div>
		   		<div class="box" data="5">
		   			<a href="#"  class="img-product-small">
			   			<img src="<?php echo base_url();?>/assets/img/product-6.png">
			   			<span>Sensitive</span>
		   			</a>
		   		</div>
		   		<div class="box" data="6">
		   			<a href="#"  class="img-product-small">
			   			<img src="<?php echo base_url();?>/assets/img/product-7.png">
			   			<span>Epic</span>
			   		</a>
		   		</div>
		   		<div class="box" data="7">
		   			<a href="#"  class="img-product-small">
			   			<img src="<?php echo base_url();?>/assets/img/product-8.png">
			   			<span>Zero</span>
		   			</a>
		   		</div>
		   </div>
		   
			<a href="<?php echo base_url();?>ingame">
            	<div class="btn-thty" ></div>
        	</a>
		</div>
		
	</div>
	<div class="menu">
		<ul>
			<li><a href="<?php echo base_url();?>video">Các Khóa Học <br> H.V.D.Y</a></li>
			<li><a href="<?php echo base_url();?>tophocvien">Top Học Viện<br> Xuất Sắc</a></li>
			<li class="bonus"> <img src="<?php echo base_url();?>/assets/img/bonus.png"><a href="<?php echo base_url();?>giaithuong">Giải <br> Thưởng</a></li>
			<li><a href="<?php echo base_url();?>thele">Thể Lệ<br> Chương Trình</a></li>
		</ul>
		<a href="#" class="share-face" id="fbsharebutton"></a>
	</div>
	<div id="product-detail" title="Basic dialog" style="display: none;">
			 	<ul class="bxslider">
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-1.png">
					 	<div class="info-product">
					 		<span class="title">Classic</span>
					 		<div class="info-product-detail">
					 			<p>• Bề mặt láng</p>
								<p>• Dáng thẳng, đầu núm cao su</p>
								<p>• Màu tự nhiên</p>
								<p>• Rộng 52mm</p>
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-2.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Fantasy.png">
					 		<span class="title">Fantasy</span>
					 		<div class="info-product-detail">
					 			<p>• Bề mặt có gân giúp bùng nổ khoái cảm hơn</p>
					 			<p>• Dáng thẳng, đầu núm cao su</p>
								<p>• Màu tự nhiên</p>
								<p>• Rộng 52mm</p>					
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-3.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Strawberry.png">
					 		<span class="title">Strawberry</span>
					 		<div class="info-product-detail">
					 			<p>• Hương dâu dịu nhẹ, nâng niu cảm xúc</p>
					 			<p>• Dáng thẳng, đầu núm cao su</p>
								<p>• Màu hồng</p>								
								<p>• Rộng 52mm</p>
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-4.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Lovetime.png">
					 		<span class="title">LoveTime</span>
					 		<div class="info-product-detail">			 			
								<p>• Kéo dài cuộc yêu</p>
								<p>• Dáng thẳng, đầu núm cao su</p>
								<p>• Màu tự nhiên</p>
								<p>• Rộng 52mm</p>
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-5.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Sensation.png">
					 		<span class="title">Sensation</span>
					 		<div class="info-product-detail">
					 			<p>• Bề mặt có gai nhẹ tạo cảm hứng “yêu”</p>
								<p>• Dáng thẳng, đầu núm cao su</p>
								<p>• Màu tự nhiên</p>
								<p>• Rộng 52mm</p>
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-6.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Sensitive.png">
					 		<span class="title">Sensitive</span>
					 		<div class="info-product-detail">
					 			<p>• Bề mặt có gai nhẹ tạo cảm hứng “yêu”</p>
					 			<p>• Dáng thẳng, đầu núm cao su</p>
								<p>• Màu tự nhiên</p>
								<p>• Rộng 52mm</p>					
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-7.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Epic.png">
					 		<span class="title">Epic</span>
					 		<div class="info-product-detail">
					 			<p>• Kéo dài “cuộc yêu”</p>
					 			<p>• Bề mặt có gai gia tăng hưng phấn</p>
								<p>• Dáng thẳng, đầu núm cao su</p>								
								<p>• Rộng 52mm</p>
					 		</div>
					 	</div>
			 		</li>
			 		<li>
			 			<img class="img-product" src="<?php echo base_url();?>/assets/img/product-8.png">
					 	<div class="info-product">
					 		<img src="<?php echo base_url();?>/assets/img/icon_product/Zero.png">
					 		<span class="title">Zero</span>
					 		<div class="info-product-detail">
					 			
								<p>• Công nghệ siêu mỏng 0,033mm</p>
								<p>• Bề mặt láng</p>
								<p>• Màu tự nhiên</p>
								<p>• Rộng 52mm</p>
					 		</div>
					 	</div>
			 		</li>
			 	</ul>
			</div>
</div>
<!-- /. Main -->