<div class="force-overflow" style="height: 450px;">
	<table class="table-top">
		<tr class="border-top-tr" >
			<td class="border-top-td-stt" >STT</td>
			<td class="border-top-td-ten" style="text-align:center !important">TÊN</td>
			<td class="border-top-td-diem">ĐIỂM</td>
		</tr>

		<?php if ( ! empty( $winersDay ) ) :
			$index = 1;
		?>
			<?php foreach ( $winersDay as $key => $player ) :
			?>
				<tr class="border-top-tr">
					<td class="border-top-td-stt"><div class="border-top-td-stt-bg"><?php echo $index++; ?></div></td>
					<td class="border-top-td-ten"><img style="margin:0px 10px 0px ;" src="<?php echo $player->avatar; ?>" height="27"><?php echo $player->name; ?></td>
					<td class="border-top-td-diem"><?php echo $player->score; ?></td>
				</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	</table>
</div>
