<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Waitting extends CI_Controller {

	public function __construct(){
		parent::__construct();

        // To use site_url and redirect on this controller.
        $this->load->model( 'player' );

	}

	public function index(){
		$data['main_content'] = 'site/waitting';
        $this->load->view('includes/template', $data);
	}
}
