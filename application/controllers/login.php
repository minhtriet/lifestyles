<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();

        // To use site_url and redirect on this controller.
        $this->load->model( 'player' );

	}

	public function index(){

		$this->load->library('facebook/facebook');
		$user = $this->facebook->get_user();
        if ($user) {
            try {
                $data['user_profile'] = $user;
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }

        if ( $user ) {
        	
            $fid = $data['user_profile']['id'];
            $img = file_get_contents('https://graph.facebook.com/' . $fid . '/picture?type=large');
            $file = dirname( __FILE__ ).'/../../assets/img/avatar/' . $fid . '.jpg';
            file_put_contents($file, $img);

        	$player = array(
        			'fbId' => $data['user_profile']['id'],
        			'email' => $data['user_profile']['email'],
        			'name' =>  $data['user_profile']['first_name']. ' ' . $data['user_profile']['last_name'] ,
        			'avatar' => base_url() . 'assets/img/avatar/' . $fid . '.jpg'
        		);

            $this->session->set_userdata( 'fbId', $player['fbId'] );

        	$this->player->add( $player );
            
            if( ! empty( $_GET['code'] ) ) {
                exit("<script>window.top.location.replace('https://www.facebook.com/pages/Test-for-LUS-Course/1617977388421453?sk=app_1587475418137530&ref=page_internal');</script>");
            }
            
            redirect( 'site' );

        } else {
            $login_url = $this->facebook->login_url(array(
                'redirect_uri' => site_url('login'), 
                'scope' => array("email") // permissions here
            ));

            exit("<script>window.top.location.replace('$login_url');</script>");

        }
	}

    public function logout(){

        $this->load->library('facebook/facebook');

        // Logs off session from website
        // $this->facebook->destroySession();

        // Make sure you destory website session as well.
        $this->session->sess_destroy();

        redirect('login');
    }

}
