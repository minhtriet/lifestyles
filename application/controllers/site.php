<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct() {

		parent::__construct();

        // To use site_url and redirect on this controller.
        $this->load->model( 'player' );
        $this->load->model( 'playperiod' );

        if ( ! $this->session->userdata('fbId') ) {

            redirect('login');
        }
	}

    public function index() {
        //load the view
        $data['main_content'] = 'site/index';
        $this->load->view('includes/template', $data);
    }

    public function thuthach() {

        //load the view
        $data['main_content'] = 'site/thuthach';
        $this->load->view('includes/template', $data);
    }

    public function giaithuong() {

        //load the view
        $data['main_content'] = 'site/giaithuong';
        $this->load->view('includes/template', $data);
    }

    public function tophocvien() {

        $data['top5'] = $this->playperiod->getTop5();
        $data['top10'] = $this->playperiod->getWinerOnDay( 10 );
        $data['winersDay'] = $this->playperiod->getWinerOnDay();

        //load the view
        $data['main_content'] = 'site/tophocvien';
        $this->load->view('includes/template', $data);
    }

    public function video() {

        //load the view
        $data['main_content'] = 'site/video';
        $this->load->view('includes/template', $data);
    }

    public function ingame() {

        list( $questions, $answers ) = $this->playperiod->getQuestions();
        $data['questions'] = $questions;
        $data['answers'] = $answers;
        $data['completed'] = $this->playperiod->completed();
        // $data['totalPlayTimes'] = $this->playperiod->getTotalPlayTimes();
        $data['totalPlayTimes'] = 1;
        $data['kq'] = array( 'knowledgePoints' => 0, 'bonusPoints' => 0, 'answeredCorrect' => 0 );
        $data['isPosted'] = 0;
        
        if ( $this->session->userdata( 'isPosted') ) {

            $data['isPosted'] = 1;
            $data['kq'] = $this->session->userdata( 'kq' );
            $this->session->set_userdata( 'isPosted', null );
            // $this->session->set_userdata( 'kq', null );
        }
        //load the view
        $data['main_content'] = 'site/ingame';
        $this->load->view('includes/template', $data);
    }

    public function thele() {

        //load the view
        $data['main_content'] = 'site/thele';
        $this->load->view('includes/template', $data);
    }

    public function chungnhan(){

        $data = $this->playperiod->certify();
        $data['main_content'] = 'site/chungnhan';
        $this->load->view('includes/template', $data);
    }
    public function waitting(){
        $data['main_content'] = 'site/waitting';
        $this->load->view('includes/template', $data);
    }

    public function topHocVienPopup() {

        $date = $this->input->post('date');

        $data['winersDay'] = $this->playperiod->getWinerOnDay( 0, $date );

        $data['main_content'] = 'site/_tophocvien';
        
        echo $this->load->view('includes/template-ajax', $data, TRUE);
    }

    public function prints() {

        $files = $this->input->post('img_val');

        //Get the base-64 string from data
        $filteredData = substr( $files, strpos( $files, ",") + 1 );

        //Decode the string
        $unencodedData = base64_decode( $filteredData );

        $filename = strtotime('now') . rand( 100, 1000 );

        $path = dirname( __FILE__ ).'/../../assets/img/prints/' .$filename.'.png';
        //Save the image
        file_put_contents( $path, $unencodedData);
        
        echo json_encode( array( 'img' => 'http://app.appstruc.com/assets/img/prints/' .$filename.'.png' ) );
    }
}
