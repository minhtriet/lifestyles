<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Players extends CI_Controller {

	public function __construct() {

		parent::__construct();

        // To use site_url and redirect on this controller.
        $this->load->model( 'player' );
        $this->load->model( 'playperiod' );

	}

	public function update() {

        $this->player->edit( $this->input->post());

        redirect( 'site/ingame');
    }

    public function setScore() {

        $this->playperiod->add( $this->input->post() );
      
        $this->session->set_userdata( 'isPosted', TRUE );

        redirect( 'ingame' );
    }

}
