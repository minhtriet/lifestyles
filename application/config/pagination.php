<?php
	//pagination settings
	$config['config'] = array(
		'per_page' => 20,
		'use_page_numbers' => TRUE,
		'num_links' => 20,
		'full_tag_open' => '<ul>',
		'full_tag_close' => '</ul>',
		'num_tag_open' => '<li>',
		'num_tag_close' => '</li>',
		'cur_tag_open' => '<li class="active"><a>',
		'cur_tag_close' => '</a></li>',
	);
?>